#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
| author: Belal HMEDAN
| LIG lab/ Marvin Team, France, 2023.
| System State Controller.
"""
import json
import importlib
import logging
import rospy
import rospkg
from std_msgs.msg import String, Bool, Int16
from std_srvs.srv import SetBool, SetBoolResponse
from geometry_msgs.msg import Point
from yumi_rapid_interface.srv import PickPlaceRAPID
from ros_planning.srv import UpdatePlan, Act
from state_controller.srv import ActorChange, ActorChangeResponse

class StateController:
    def __init__(self):
        """ """
        self.old_model = ""
        self.old_lego_map = ""
        self.model_validated = None
        self.lego_map_validated = None
        self.old_plan = []
        self.plan = []
        self.accum_plan = {}
        self.yumi_motion_status = False
        self.mov_fini = True
        self.human_hand = False
        self.arm_selected = None
        self.pattern = None
        self.speed = 170
        self.start_signal = False
        self.assembly_errors = []
        self.occupied_positions = []
        self.executed_legos = []
        self.planning_level = 1
        
        rospy.init_node("state_controller", anonymous=True, log_level=rospy.INFO)
        self.acc_plan_pub = rospy.Publisher("accumulated_plan", String, queue_size=1)

        ## Logging Settings
        importlib.reload(logging)
        rospack = rospkg.RosPack()
        path_ = rospack.get_path("state_controller")
        ## Set logging configurations
        logging.basicConfig(
            filename=path_ + "/database/state_controller.log",
            level=logging.INFO,
            filemode="w",
            format="%(asctime)s.%(msecs)03d, %(levelname)s, %(message)s",
            datefmt="%Y-%m-%d, %H:%M:%S",
        )
        self.stateReader()

    def key2pos(self, s):
        """
        Function: key2pos, to obtain x, y cell coordinates from string of shape 'p_xx_yy_z'.
        ---
        Parameters:
        @param: s, string of shape 'p_xx_yy_z'.
        ---
        @return: list, [x, y, z], the cell coordinates.
        """
        o = s.split("_")
        if len(o) == 4:
            return [int(o[1]), int(o[2]), int(o[3])]
        elif len(o) == 3:
            return [int(o[1]), int(o[2])]
        else:
            logging.error("key2pos wrong s: {}".format(s))

    def planPublisher(self, event=None):
        """ """
        self.acc_plan_pub.publish(json.dumps(self.accum_plan))

    def stateReader(self):
        """
        subscribe to model/lego_map/yumi_motion_status
        """
        ## From Vision System
        rospy.Subscriber("model", String, self.modelRec)
        rospy.Subscriber("lego_map", String, self.legoMapRec)
        # rospy.Subscriber("assembly_errors", String, self.assemblyErrorsRec)
        rospy.Subscriber("yumi_motion_status", Bool, self.motionStatusRec)
        rospy.Subscriber("planning_level", Int16, self.planningLevRec)

        ## From the GUI
        rospy.Subscriber("user_arm", String, self.userArmRec)
        rospy.Subscriber("pattern", String, self.patternRec)
        rospy.Subscriber("speed", Int16, self.speedRec)
        rospy.Subscriber("start_signal", Bool, self.startRec)
        ## Services
        rospy.Service("validate_vision_info", SetBool, self.validVision)
        rospy.Service("change_actor", ActorChange, self.changeActor)
        ## Timer for Publisher
        rospy.Timer(rospy.Duration(1.0), self.planPublisher)

        rospy.spin()

    ## Subscriber CallBacks
    def modelRec(self, data):
        """ """
        model = json.loads(data.data)
        if model != self.old_model:
            logging.info("New Model Recieved")
            self.occupied_positions = []
            self.old_model = model
            for pos_xx_yy in model:
                for z_ in model[pos_xx_yy]:
                    pos_xx_yy_z = pos_xx_yy+'_'+str(z_)
                    self.occupied_positions.append(pos_xx_yy_z)
            if self.model_validated is None:
                self.model_validated = model

    def legoMapRec(self, data):
        """ """
        lego_map = json.loads(data.data)
        if lego_map != self.old_lego_map:
            logging.info("New Lego Map Recieved")
            self.old_lego_map = lego_map
            if self.lego_map_validated is None:
                self.lego_map_validated = lego_map

    def assemblyErrorsRec(self, data):
        """ """
        assembly_errors = json.loads(data.data)
        if assembly_errors != self.assembly_errors:
            logging.warning("Assembly Errors Detected: {}".format(data.data))
            self.assembly_errors = assembly_errors

    def motionStatusRec(self, msg):
        """ """
        if self.yumi_motion_status != msg.data:
            logging.info("Robot Motion Status Changed to: {}".format(msg.data))
            self.yumi_motion_status = msg.data
            if not self.yumi_motion_status:
                self.mov_fini = True
                logging.info("Action Execution Finished!")
                logging.info(
                    "Acc Plan before Executing the Next Action: {}".format(
                        self.accum_plan
                    )
                )
                # self.executionScheduler()
            else:
                self.mov_fini = False

    def planningLevRec(self, data):
        """ """
        self.planning_level = data.data

    def userArmRec(self, data):
        """ """
        if self.arm_selected != data.data:
            logging.warning("User Arm Selected: {}".format(data.data))
            self.arm_selected = data.data

    def patternRec(self, data):
        """ """
        if self.pattern != data.data:
            logging.warning("Pattern Selected: {}".format(data.data))
            self.pattern = data.data
            if(self.pattern=="complex"):
                self.robot_stock = {
                    "wc1": "p_00_03_0",
                    "wc2": "p_00_19_0",
                    "rc1": "p_04_01_0",
                    "rc2": "p_04_21_0",
                    "bc1": "p_02_03_0",
                    "bc2": "p_02_19_0",
                    "oc1": "p_00_05_0",
                    "oc2": "p_00_17_0",
                    "yb1": "p_00_00_0",
                    "yb2": "p_00_21_0",
                    "bb1": "p_02_00_0",
                    "bb2": "p_02_21_0",
                }
            elif(self.pattern=="simple"):
                self.robot_stock = {
                    "bb1": "p_00_00_0",
                    "bb2": "p_00_21_0",
                    "rb1": "p_02_00_0",
                    "rb2": "p_02_21_0"
                }
                

    def speedRec(self, data):
        """ """
        if self.speed != data.data:
            logging.warning("Robot Speed Changed to: {}".format(data.data))
            self.speed = data.data

    def startRec(self, msg):
        """ """
        if self.start_signal != msg.data:
            logging.warning("Start Signal Changed to: {}".format(msg.data))
            self.start_signal = msg.data
            # Planning here has a Blocking behavior (Service call)
            if msg.data:
                self.planningInterface()

    ## Service Servers

    def validVision(self, req):
        """ """
        model_valid = req.data
        logging.info("Vision Model Validation: {}".format(req.data))

        if model_valid:
            self.model_validated = self.old_model
            self.lego_map_validated = self.old_lego_map
            self.planningInterface()
        else:
            pass

        return SetBoolResponse(
            success=True, message="Bool was set to: {}".format(req.data)
        )

    def changeActor(self, req):
        """ """
        lego = req.data
        logging.warning(
            "Action changed, lego: {} now is assigned to the operator.".format(req.data)
        )
        self.accum_plan[lego].update({"actor": "operator"})
        return ActorChangeResponse(True)

    ## Service Client
    def planningInterface(self):
        """
        send the lego_map to the planner to get a plan
        """
        logging.info("Waiting for planning_service")
        rospy.wait_for_service("planning_service")
        if not (
            self.model_validated is None
            or self.arm_selected is None
            or self.pattern is None
        ):
            try:
                logging.info("Planning Request Initiated")
                planning_service = rospy.ServiceProxy("planning_service", UpdatePlan)
                response = planning_service(
                    json.dumps(self.model_validated), self.arm_selected, self.pattern, self.planning_level
                )
                logging.info("Planning Request Was Done {}".format(response.plan))
                if self.plan != response.plan:
                    logging.warning("New Plan {}".format(self.plan))
                    self.plan = response.plan
                    self.planAccumulator()
                self.executionScheduler()
            except rospy.ServiceException as e:
                logging.error("Planning Request Failed: %s" % e)
        else:
            logging.error(
                "Planning is not possible pattern: {}, selected arm: {}, validated model: {}".format(
                    self.pattern, self.arm_selected, self.model_validated
                )
            )
 
    def checkUnderPos(self, place_pt):
        """ """
        z_place = self.key2pos(place_pt)[-1]

        logging.info("Under Position: {}, \nOccupied Positions: {}".format(place_pt, self.occupied_positions))
        if(z_place==0):
            return True
        elif(place_pt in self.occupied_positions):
            return True
        else:
            return False
        
    def checkFeasibilty(self, place_pt):
        """ """
        # return true if feasible, false if not
        if(self.checkUnderPos(place_pt)):
            return True
        else:
            return False
        
    def planAccumulator(self):
        """ """
        # remove pending actions from the old plan
        if bool(self.accum_plan):
            for lego in list(self.accum_plan.keys()):
                if self.accum_plan[lego]["status"] == "pending":
                    logging.info(
                        "Removing Pending Action {} from Accumulated Plan".format(lego)
                    )
                    self.accum_plan.pop(lego)
        else:
            logging.warning("Empty Accumulated Plan!")

        # Add new (non-executed) actions to the accumulated plan
        if self.plan:
            logging.info("Started Plan Accumulation: {}".format(self.plan))
            accum_plan = json.loads(self.plan)

            for action in accum_plan:
                actor = action[0]
                lego_ = action[1]
                place_pt = action[2]
                status = "pending"
                if actor == "robot_arm":
                    pick_pt = self.robot_stock[lego_]
                else:
                    pick_pt = self.old_lego_map[lego_][0] + "_0"
                    x_pick = self.key2pos(pick_pt)[0]
                    if(x_pick<8):
                        status = "executed"
                rot_ = False
                if place_pt == "p_03_11_0":
                    rot_ = True
                
                feasible = self.checkFeasibilty(place_pt)

                if (not lego_ in self.accum_plan) and (
                    not lego_ in self.executed_legos
                ):
                    logging.info("New Action ({}) Accumulated".format(lego_))
                    self.accum_plan[lego_] = {
                        "actor": actor,
                        "pick_": pick_pt,
                        "place_": place_pt,
                        "rotatoin": rot_,
                        "status": status,
                        "id": len(self.accum_plan),
                        "feasible": feasible,
                    }
                elif lego_ in self.accum_plan:
                    # Accumulate Executed Actiond from the old plan
                    logging.warning("Action ({}) Exists, not Added!".format(lego_))
                elif lego_ in self.executed_legos:
                    logging.warning("Action ({}) Has been executed!".format(lego_))
        else:
            logging.warning("Received Empty Plan: {}".format(self.plan))

    def executeAction(self, pick_pt, place_pt):
        """
        execute action and get the execution status: success/timeout
        TODO: To get Failure status from the motion change timeout
        """
        rospy.wait_for_service("pick_place_rapid")
        try:
            logging.info(
                "Executing Action Pick {}, Place {}, with speed: {}".format(
                    pick_pt, place_pt, self.speed
                )
            )
            pp_rapid = rospy.ServiceProxy("pick_place_rapid", PickPlaceRAPID)
            if self.arm_selected == "left":
                arm = True
            elif self.arm_selected == "right":
                arm = False
            else:
                logging.error("Robot Arm is not Selected: {}".format(self.arm_selected))
            pp_rapid(pick_pt, place_pt, self.speed, arm)
            logging.info("Action Executed.")
        except rospy.ServiceException as e:
            logging.error("Action Execution Failed: %s" % e)

    def executionScheduler(self):
        """ """
        # change the status of the action to current in the accumulated plan
        if (
            self.start_signal
            and not (self.pattern is None)
            and not (self.arm_selected is None)
            and bool(self.accum_plan)
        ):
            logging.info("Started Execution Loop of Plan: {}".format(self.accum_plan))
            # 
            rospy.wait_for_service("remove_action")
            remove_action = rospy.ServiceProxy("remove_action", Act)

            for lego_ in self.accum_plan:
                if self.accum_plan[lego_]["status"] != "executed":
                    logging.info("Accumulated Plan: {}".format(self.accum_plan))
                    logging.warning("Executing Action: {}".format(lego_))
                    if self.accum_plan[lego_]["actor"] == "robot_arm":
                        if (
                            self.accum_plan[lego_]["status"] == "pending"
                            and self.mov_fini
                            and self.accum_plan[lego_]["feasible"]
                        ):
                            # Tell the Planner to remove this lego.
                            remove_action(lego_)
                            #
                            self.accum_plan[lego_].update({"status": "current"})
                            self.planPublisher()
                            x0, y0, z0 = self.key2pos(self.accum_plan[lego_]["pick_"])
                            pick_ = Point(x0, y0, z0)
                            x1, y1, z1 = self.key2pos(self.accum_plan[lego_]["place_"])
                            place_ = Point(x1, y1, z1)
                            self.executeAction(pick_, place_)
                            # Wait Motion Status Change to False...
                            self.accum_plan[lego_].update({"status": "executed"})
                            self.accum_plan[lego_]["status"] == "executed"
                            self.executed_legos.append(lego_)
                            logging.info(
                                "Acc Plan after Executing the Action: {}".format(
                                    self.accum_plan
                                )
                            )
                            break
                    else:
                        pick_pt = self.accum_plan[lego_]["pick_"]
                        x_pick = self.key2pos(pick_pt)[0]
                        if x_pick < 8:
                            logging.info("Human Action ({}) was Executed".format(lego_))
                            self.accum_plan[lego_]["status"] = "executed"
                else:
                    logging.info("Skipping Executed Action ({})".format(lego_))

        else:
            logging.info(
                "Execution Conditions are not met! start: {}, Pattern: {}, Arm: {}, Plan: {}".format(
                    self.start_signal, self.pattern, self.arm_selected, self.accum_plan
                )
            )

##=============================================
if __name__ == "__main__":
    try:
        controller_ = StateController()

    except rospy.ROSInterruptException:
        logging.error("Error in the State Controller")
